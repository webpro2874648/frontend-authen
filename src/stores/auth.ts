import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import { useMessageStore } from '@/stores/message';
import { useRouter } from 'vue-router';
import { useLoadingStore } from './loading';

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const loading = useLoadingStore()
  const login = async function (email: string, password: string) {
    loading.doLoad()
    try {
      const res = await authService.login(email, password)
      console.log(res.data);
      localStorage.setItem('user', JSON.stringify(res.data.user)) //save to harddisk
      localStorage.setItem('access_token', JSON.stringify(res.data.access_token)) //save to harddisk
      messageStore.showMessage('Login Successful')
      router.replace('/')
    } catch (e: any) {
      console.log(e.message)
      messageStore.showMessage(e.message)
    }
    loading.finish()
  }
  const logout = function () {
    localStorage.removeItem('user') //save to harddisk
    localStorage.removeItem('access_token') //save to harddisk
    router.replace('/login')
  }
  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }
  function getToken(): User | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return JSON.parse(strToken)
  }
  return { getCurrentUser, login, getToken, logout }
})
